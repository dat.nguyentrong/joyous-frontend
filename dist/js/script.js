var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.href.split('?')[1],
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

var stringInject = function(str, arr) {
  if (typeof str !== 'string' || !(arr instanceof Array)) {
    return false;
  }

  return str.replace(/({\d})/g, function(i) {
    return arr[i.replace(/{/, '').replace(/}/, '')];
  });
};

var Alert = (function(){
  var error = function(text){
    $.toast({
      text: text,
      bgColor : '#dc3545',
      hideAfter : 5000,
      position : 'top-center',
    });
  };

  var success = function(text){
    $.toast({
      text: text,
      bgColor : '#28a745',
      hideAfter : 5000,
      position : 'top-center',
    });
  };

  return {
    error: error,
    success: success
  };
})();

var TEMPLATES = {};

TEMPLATES.LANDING_PAGE = '<div class="container"><div class="text-center logo pt-4 pb-4"><img src="http://graduate.joyousasia.com/img/usr/logo-joyous-login.gif" alt="Joyous Logo"/></div><div class="text-center"><h1>LANDING PAGE</h1><button type="button" class="btn btn-default" id="login-btn">LOG IN</button></div></div>';
TEMPLATES.HOME_PAGE = '<div class="container pt-4 pb-4"> <div class="row row-p-none"> <div class="col-md-3"> <ul class="sidebar-menu"> <li class="has-child active"> <a href="#" id="mainEvent">EVENT INFORMATION</a> <ul class="sidebar-submenu"> </ul> </li><li> <a href="#">EVENT REGISTRATION</a> </li><li> <a href="#">EVENT PHOTO & VIDEO</a> </li></ul> </div><div class="col-md-9"> <div class="main-content"> <div class="page-header"> EVENT INFORMATION </div><div class="page-content"> </div><div class="page-footer clearfix"> <div class="float-left"> <a href="#" class="btn btn-secondary">Back</a> </div><div class="float-right"> <a href="#" class="btn btn-secondary">Print</a> <a href="#" class="btn btn-secondary">Next</a> </div></div></div></div></div></div>';
TEMPLATES.EVENT_INFO ='<div id="cphContent_ucUsrEventInformation_pnlEventBasicInfo"> <table cellpadding="0" cellspacing="0" class="formTbl"> <tbody><tr> <td class="tdLabel tdSpacer"> <span id="cphContent_ucUsrEventInformation_lblEventVenue">Venue</span> </td><td class="valueVenue">{0}</td></tr><tr> <td class="tdLabel tdSpacer"> <span id="cphContent_ucUsrEventInformation_lblEventVenueMap">Venue Map</span> </td><td> <a id="cphContent_ucUsrEventInformation_hypEventVenueMap" title="{1}" class="linkBlack" href="{1}" target="_blank">{1}</a> </td></tr><tr> <td class="tdLabel tdSpacer"> <span id="cphContent_ucUsrEventInformation_lblEventDate">Date</span> </td><td>{2}</td></tr><tr> <td class="tdLabel tdSpacer"> <span id="cphContent_ucUsrEventInformation_lblEventTime">Event Time</span> </td><td>{3}</td></tr><tr> <td class="tdLabel tdSpacer"> <span id="cphContent_ucUsrEventInformation_Label1">Reporting Time</span> </td><td>{4}</td></tr><tr> <td class="tdSpacer">&nbsp;</td><td class="tdSpacer">&nbsp;</td></tr><tr> <td colspan="2"> <span class="spanLastCall">( Registration closes on 01-Nov-2019 )</span> </td></tr><tr> <td class="tdSpacer">&nbsp;</td><td class="tdSpacer">&nbsp;</td></tr></tbody></table> </div>';
TEMPLATES.LOGIN_PAGE = '<div class="container"><div class="text-center logo pt-4 pb-4"><img src="http://graduate.joyousasia.com/img/usr/logo-joyous-login.gif" alt="Joyous Logo"/></div><div class="login-container py-4 px-5 bordered"><div class="login-header"><h2>GRADUATE LOGIN</h2><p><a>Sign up your Family Portrait NOW and get a 8R Single Portrait for FREE</a> Call now @ 68447142</p><hr/></div><form id="login" class="form"><p>Kindly login with ID and Password provided by school.</p><div class="form-inner"><div class="form-group row"> <label for="inputID" class="col-sm-2 col-form-label">Login ID:</label> <div class="col-sm-10"> <input type="text" class="form-control" id="inputID" placeholder="" required> </div></div><div class="form-group row"> <label for="inputPassword" class="col-sm-2 col-form-label">Password:</label> <div class="col-sm-10"> <input type="password" class="form-control" id="inputPassword" placeholder="" required> </div></div></div><div class="text-right"><input class="btn btn-light" type="submit" value="Login"/></div></form></div></div>';
var root = null;
var useHash = true;
var hash = '#';
var router = new Navigo(root, useHash, hash);
var LOGIN_URL = '';

$(document).ready(function(){

	var setLoginContent = function(){
		$("#main").html(TEMPLATES.LOGIN_PAGE);
		$('body').on('submit', '#login', function(e){
			e.preventDefault();
			var username = $('inputID').val();
			var password = $('inputPassword').val();
			//call ajax to login, if success redirect to Event page
			$.ajax({
				url : LOGIN_URL,
				method: 'post',
				data: JSON.stringify({username: username, password: password}),
				success : function (data) {
					event = data.responseData;
					router.navigate('event?code=SIHTS&email=abc@def.com');
				},
				error: function () {
					Alert.error('Login error');
				}
			});
		});
	}

	var setLandingContent = function(){
		$("#main").html(TEMPLATES.LANDING_PAGE);
		$('body').on('click', '#login-btn', function(e){
			router.navigate('login');
		});
	}
	
	var setEventContent = function(){
		var baseAPI = 'http://joyous-spring.nhatphuong.io/joyous/public/event/info';
		var code = getUrlParameter('code');
		var email = getUrlParameter('email');
		var event;

		if(code && email){
			$.ajax({
				url : baseAPI+'?code='+code+'&email='+email,
				success : function (data) {
					event = data.responseData;
					renderSubUl();
					mainEventClicked();
				}
			});
		}
		$("#main").html(TEMPLATES.HOME_PAGE);

		var mainEventClicked = function(e){
			if (e){
				e.preventDefault();
			}
			var date = moment(event.date).format('DD-MMM-YYYY');
			var content = stringInject(TEMPLATES.EVENT_INFO, [event.venue, event.venueWebsite, date, event.time, '-']);
			$(".page-content").html(content);
		}

		var renderSubUl = function(){
			var subEventModel = '<li><a href="#" data-id="{0}" class="sub-item">{1}</a></li>';
			var eventsHtml = '';
			if (event.contents && event.contents.length){
				event.contents.forEach(function(entry, index) {
					var event = stringInject(subEventModel, [index, entry.title]);
					eventsHtml += event;
				});
				$('.sidebar-submenu').html(eventsHtml);
			}
		};

		$('body').on('click', '.sidebar-submenu li a', function(e){
			e.preventDefault();
			$('.sub-item').removeClass('active');
			$(this).addClass('active');
			var index = $(this).attr('data-id');
			var subEvent = event.contents[index];
			$(".page-content").html(subEvent.content);
		});

		$('body').on('click', '#mainEvent', function(e){
			$('.sub-item').removeClass('active');
			mainEventClicked(e);
		});
	}

	router
	.on({
		'login': function () {
			setLoginContent();
		},
		'event': function () {
			setEventContent()
		},
		'*': function () {
			setLandingContent();
		}
	})
	.resolve();

});