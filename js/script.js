var root = null;
var useHash = true;
var hash = '#';
var router = new Navigo(root, useHash, hash);
var LOGIN_URL = '';

$(document).ready(function(){

	var setLoginContent = function(){
		$("#main").html(TEMPLATES.LOGIN_PAGE);
		$('body').on('submit', '#login', function(e){
			e.preventDefault();
			var username = $('inputID').val();
			var password = $('inputPassword').val();
			//call ajax to login, if success redirect to Event page
			$.ajax({
				url : LOGIN_URL,
				method: 'post',
				data: JSON.stringify({username: username, password: password}),
				success : function (data) {
					router.navigate('event?code=SIHTS&email=abc@def.com');
				},
				error: function () {
					Alert.error('Login error');
				}
			});
		});
	};

	var setLandingContent = function(){
		$("#main").html(TEMPLATES.LANDING_PAGE);
		$('body').on('click', '#login-btn', function(e){
			router.navigate('login');
		});
	};
	
	var setEventContent = function(){
		var baseAPI = 'http://joyous-spring.nhatphuong.io/joyous/public/event/info';
		var code = getUrlParameter('code');
		var email = getUrlParameter('email');
		var event;

		if(code && email){
			$.ajax({
				url : baseAPI+'?code='+code+'&email='+email,
				success : function (data) {
					event = data.responseData;
					renderSubUl();
					mainEventClicked();
				}
			});
		}
		$("#main").html(TEMPLATES.HOME_PAGE);

		var mainEventClicked = function(e){
			if (e){
				e.preventDefault();
			}
			var date = moment(event.date).format('DD-MMM-YYYY');
			var content = stringInject(TEMPLATES.EVENT_INFO, [event.venue, event.venueWebsite, date, event.time, '-']);
			$(".page-content").html(content);
		}

		var renderSubUl = function(){
			var subEventModel = '<li><a href="#" data-id="{0}" class="sub-item">{1}</a></li>';
			var eventsHtml = '';
			if (event.contents && event.contents.length){
				event.contents.forEach(function(entry, index) {
					var event = stringInject(subEventModel, [index, entry.title]);
					eventsHtml += event;
				});
				$('.sidebar-submenu').html(eventsHtml);
			}
		};

		$('body').on('click', '.sidebar-submenu li a', function(e){
			e.preventDefault();
			$('.sub-item').removeClass('active');
			$(this).addClass('active');
			var index = $(this).attr('data-id');
			var subEvent = event.contents[index];
			$(".page-content").html(subEvent.content);
		});

		$('body').on('click', '#mainEvent', function(e){
			$('.sub-item').removeClass('active');
			mainEventClicked(e);
		});
	};

	router
	.on({
		'login': function () {
			setLoginContent();
		},
		'event': function () {
			setEventContent()
		},
		'*': function () {
			setLandingContent();
		}
	})
	.resolve();

});