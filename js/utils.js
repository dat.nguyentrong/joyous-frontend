var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.href.split('?')[1],
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

var stringInject = function(str, arr) {
  if (typeof str !== 'string' || !(arr instanceof Array)) {
    return false;
  }

  return str.replace(/({\d})/g, function(i) {
    return arr[i.replace(/{/, '').replace(/}/, '')];
  });
};

var Alert = (function(){
  var error = function(text){
    $.toast({
      text: text,
      bgColor : '#dc3545',
      hideAfter : 5000,
      position : 'top-center',
    });
  };

  var success = function(text){
    $.toast({
      text: text,
      bgColor : '#28a745',
      hideAfter : 5000,
      position : 'top-center',
    });
  };

  return {
    error: error,
    success: success
  };
})();
