var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

gulp.task('sass', function () {
  return gulp.src('./sass/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('scripts', function() {
  return gulp.src(['./js/utils.js', './js/templates.js', './js/script.js'])
    .pipe(concat('script.js'))
    .pipe(minify())
    .pipe(gulp.dest('./dist/js/'))
});

gulp.task('build', gulp.parallel('sass', 'scripts'));

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: "./",
      port: 3010
    }
  });
});